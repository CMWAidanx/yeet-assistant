const storage = require("node-persist");
const discord = require("discord.js");
module.exports = {
    readSetting: async function(guild, settingName) {
        let guildid = guild.id;
        await storage.init();
        return await storage.getItem(guildid + settingName);
    },
    setSetting: async function(guild, settingName, settingValue) {
        let guildid = guild.id;
        await storage.setItem(guildid + settingName, settingValue);       
    },
    getSettingsUI(setting, value) {
        let ui = new discord.RichEmbed()
        .setAuthor("Yeet Assistant Settings")
        .setTitle("Setting: " + setting)
        .setDescription("Value: " + value)
        .setFooter("Yeet Assistant")
        .setColor(0x4e605b);
        return ui;
    }
}