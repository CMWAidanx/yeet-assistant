// Simplified Action Summaries, with less information but less clutter. 

module.exports = {
  sendSummary: function(action, affected, channel) {
      channel.sendMessage("✅ " + action + " ➡️ " + affected);
  }  
};