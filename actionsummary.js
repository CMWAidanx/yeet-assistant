const discord = require("discord.js");
const settings = require("./settings");
const sas = require ("./beta/s-as/s-as")
module.exports = {

    sendSummary: function(action, status, details, affected, channel, color, icon, guild) {
            if (settings.readSetting(guild, "useSimplified") === "true") {
                sas.sendSummary(action, affected, channel);
                return;
            } else {
                var summary = new discord.RichEmbed()
                .setAuthor("Action Summary", icon)
                .setTitle("Action: " + action)
                .setDescription("Status: " + status + "\nDetails: " + details + "\nAffected: " + affected)
                .setColor(color);
                channel.sendEmbed(summary);
        }
    }
}