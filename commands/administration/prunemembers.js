const commando = require("discord.js-commando");
const as = require("../../actionsummary");

class PruneMembers extends commando.Command {
    constructor(client) {
        super(client, {
            name: "prunemembers",
            group: "administration",
            memberName: "prunemembers",
            description: "Prunes members that have been inactive for longer than the specified period."
        });
    }
    async run(message, args) {
        if (!message.member.hasPermission("KICK_MEMBERS")) {
            message.reply("You don't have permission to do that.");
            return;
        }
        let words = args.split(' ');
        if (words[0] == null) {
            message.reply("I need to know the period of inactivity to prune by.");
            return;
        }
        message.guild.pruneMembers(parseInt(words[0]))
        .then(pruned => as.sendSummary("Prune Members", "done",  `Pruned members from the last ${words[0]} days`, `${pruned} members`, message.channel, 0x21ff00, null, message.guild));
    }
}
module.exports = PruneMembers;
// gay