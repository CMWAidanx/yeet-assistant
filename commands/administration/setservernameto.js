const commmando = require("discord.js-commando");
const discord = require("discord.js");
class SetServerNameTo extends commmando.Command {
    constructor (client) {
        super(client, {
            name: "setservernameto",
            group: "administration",
            memberName: "setservernameto",
            description: "A command that sets the name of the server"
        });

    }
    async run(message, args) {
        if (!message.member.hasPermission("MANAGE_GUILD")) {
            console.log("Prevented an attempt by a under-permitted user to change the server name.");
            var summary = new discord.RichEmbed()
            .setAuthor("Action Summary")
            .setTitle("Action: Change Server Name")
            .setDescription("Status: failed\nReason: You lack the permissions to do that. Make sure you have the permission Manage Server and try again.")
            .setColor(0xFF0F00);
            message.channel.sendEmbed(summary);
            return;
        }
        var oldName = message.guild.name;
        let words = args.split(' ');
        let newName = words.slice(0).join(' ');
        var summary = new discord.RichEmbed()
        .setAuthor("Action Summary")
        .setTitle("Action: Change Server Name")
        .setDescription("Old Name: " + oldName + "\n New Name: " + newName + "\nStatus: done")
        .setColor(0x21ff00);
        message.channel.sendEmbed(summary);
        console.log("Changed server name from " + oldName + " to " + newName);
        message.guild.setName(newName); 
    }
}
module.exports = SetServerNameTo;