const commmando = require("discord.js-commando");
const discord = require("discord.js");
// const request = require("request");
class UrbanCommand extends commmando.Command {
    constructor (client) {
        super(client, {
            name: "urban",
            group: "fun",
            memberName: "urban",
            // ownerOnly: true,
            description: "Gets data from the urban dictionary"
        });

    }
    async run(message, args) {
                        let words = args.split(' ');
                        let reason = words.slice(0).join('+');
        message.reply("here's what the urban dictionary has to say about that.")
        message.channel.sendMessage("https://urbandictionary.com/define.php?term=" + reason);
    }
    
}
module.exports = UrbanCommand;