const commando = require('discord.js-commando');
const discord = require("discord.js");
class GuessTheMagicNumber extends commando.Command {
    constructor(client) {
        super(client, {
            name: "magicnumber",
            group: "fun",
            memberName: "magicnumber",
            description: "A fun game of guess the magic number"
        })
    }
    async run (message, args) {
        var done = false;
        message.reply("let me think of a number...");
        let actualNumber = (Math.floor(Math.random() * 10));
        message.reply("I've got it! Try and guess what it is. It's between 1 and 10.");
        var collector = new discord.MessageCollector(message.channel, m => m.author.id === message.author.id, { time: 10000 });
        console.log(collector);
        collector.on('collect', message1 => {
            if (done) {
                return;
            }
            if (parseInt(message1.content) == null) {
                message1.reply("That didn't look like a number. Which is a shame. By the way, the number was " + actualNumber + ". Thanks for playing!");
            } else if (parseInt(message1.content) == actualNumber) {
                message1.reply("you got it right! Congratulations! Thanks for playing.");
                done = true;
                collector = null;
                return;
            } else {
                message1.reply("I'm sorry, but that wasn't quite it. The number was actually " + actualNumber + "! Thanks for playing!");
                message1.reply("You guessed " + parseInt(message1.content));
                done = true;
                collector = null;
                return;
            }
        })
        
        // message = null;
        // actualNumber = null;

    }
}
module.exports = GuessTheMagicNumber;