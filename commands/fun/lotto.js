const commmando = require("discord.js-commando");
const msg = require("./messages/lotto.json");
class LottoCommand extends commmando.Command {
    constructor (client) {
        super(client, {
            name: "lotto",
            group: "fun",
            memberName: "lotto",
            description: "Will you win the lotto?"
        });

    }
    async run(message, args) {
        if (Math.floor(Math.random() * 100000000) == Math.floor(Math.random() * 100000000)) {
        message.reply(msg.win);
        } else {
            message.reply(msg.lose);
        }
        }
}
module.exports = LottoCommand;