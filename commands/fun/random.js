const commmando = require("discord.js-commando");
const discord = require("discord.js");
class RandomCommand extends commmando.Command {
    constructor (client) {
        super(client, {
            name: "random",
            group: "fun",
            memberName: "random",
            // ownerOnly: true,
            description: "Generates a random number"
        });

    }
    async run(message, args) {
        var randomNumber = new discord.RichEmbed()
        .setAuthor("Random Number", "https://cdn4.vectorstock.com/i/1000x1000/79/68/dice-icon-vector-13367968.jpg")
        .setTitle("Action Summary")
        .setDescription("Action: Random Number\nResult: " + Math.floor(Math.random() * 100))
        .setFooter("Yeet Assistant")
        .setColor(0x428ff4);
        message.channel.sendEmbed(randomNumber);
    }
    
}
module.exports = RandomCommand;