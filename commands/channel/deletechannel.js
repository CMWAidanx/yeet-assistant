const commmando = require("discord.js-commando");
const discord = require("discord.js");
class DeleteChannel extends commmando.Command {
    constructor (client) {
        super(client, {
            name: "deletechannel",
            group: "channel",
            memberName: "deletechannel",
            description: "Deletes the channel this command was sent in"
        });

    }
    async run(message, args) {
        if(!message.member.hasPermission("MANAGE_CHANNELS")) {
            var summary = new discord.RichEmbed()
            .setAuthor("Action Summary")
            .setTitle("Action: Delete Channel")
            .setDescription("Status: failed\nReason: You don't have permission")
            .setColor(0xFF0F00);
            message.channel.sendEmbed(summary);
            return;
        }
        var warning = new discord.RichEmbed()
        .setAuthor("Are you sure?", "https://cdn0.iconfinder.com/data/icons/social-messaging-ui-color-shapes/128/alert-triangle-red-512.png")
        .setTitle("This will delete the channel you are in right now. Make sure it's the correct one!")
        .setDescription("This cannot be undone.")
        .setFooter("To confirm, reply (without the prefix) 'yes'. To cancel, reply (without the prefix) 'no'.")
        .setColor(0xFF0F00);
        message.channel.sendEmbed(warning);
        const collector = new discord.MessageCollector(message.channel, m => m.author.id === message.author.id, { time: 10000 });
        console.log(collector)
        collector.on('collect', message => {
            if (message.content == "yes") {
                message.reply("Deleting channel...");
                message.channel.delete();
            } else {
                message.reply("The action was cancelled.");
            }
        })
    }
}
module.exports = DeleteChannel;