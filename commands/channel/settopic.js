const commando = require("discord.js-commando");
const discord = require("discord.js");
const as = require("../../actionsummary");
class SetTopicCommand extends commando.Command {
    constructor (client) {
        super(client, {
            name: 'settopic',
            group: 'channel',
            memberName: 'settopic',
            description: "Sets the topic of the channel you're currently in."
        });
    }
    async run(message, args) {
        let words = args.split(' ');
        let newName = words.slice(0).join(' ');
        if (!message.member.hasPermission("MANAGE_CHANNELS")) {
            as.sendSummary("Set Channel Topic", "failed", "You don't have permission", message.channel, 0x0FF0F00);
            return;
        }
        message.channel.setTopic(newName)
        .then(updated => as.sendSummary("Set Channel Topic", "done", `Channel topic is now ${updated.topic}`, "Channel", message.channel, 0x21ff00, null, message.guild))
        .catch(console.error);
    }
}
module.exports = SetTopicCommand;