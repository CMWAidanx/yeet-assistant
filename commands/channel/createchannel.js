const commando = require("discord.js-commando");
const as = require("../../actionsummary");

class CreateChannel extends commando.Command {
    constructor(client) {
        super(client, {
            name: "createchannel",
            group: "channel",
            memberName: "createchannel",
            description: "Creates a channel with the specified name"
        })
    }
    async run(message, args) {
        if (!message.member.hasPermission("MANAGE_CHANNELS")) {
            message.reply("You don't have permission to do that. If this is a mistake, contact an administrator");
            return;
        }
        let words = args.split(' ');
        let reason = words.slice(0).join('-');
        message.guild.createChannel(reason)
        .then(as.sendSummary("Create Channel", "done", "\nChannel Name: #" + reason, "Server", message.channel, 0x21ff0, null, message.guild));
    }
}
module.exports = CreateChannel;