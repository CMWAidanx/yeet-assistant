const settings = require("../../settings");
const commando = require("discord.js-commando");
const as = require("../../actionsummary");
class SettingsCommand extends commando.Command {
    constructor(client) {
        super(client, {
            name: "settings",
            group: "settings",
            memberName: "settings",
            description: "Changes settings.",
            // examples: "!settings <setting> <value>"
        });
    }
    async run (message, args) {
        let words = args.split(' ');
        // let guildID = message.guild.id;
        if (!message.member.hasPermission("MANAGE_SERVER")) {
            message.reply("You don't have permission to manage server settings.");
            return;
        }
        if (words[0] == null) {
            message.reply("What setting do you want to change? Usage is `<prefix>settings <setting> <value>.");
        } if (words [1]  == null) {
            let settingValue = settings.readSetting(message.guild, args[0]);
            if (settingValue == null) {
                message.reply("Invalid setting. Please choose a valid setting.");
            } else {
                message.channel.sendEmbed(settings.getSettingsUI(words[0], settingValue));
            }
        } else {
            settings.setSetting(message.guild, words[0], words[1])
            .then(as.sendSummary("Change Setting", "done", "\nSetting: " + words[0] + "\nNew Value: " + words[1], "Server", message.channel, 0x00ff43));
        }
    }
}
module.exports = SettingsCommand;