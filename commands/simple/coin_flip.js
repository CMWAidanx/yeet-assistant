const commando = require("discord.js-commando");

class CoinFlipCommand extends commando.Command {
    constructor(client) {
        super(client,{
            name: 'flip',
            group: 'simple',
            memberName: 'flip',
            description: 'Literally flips a non-literal coin'
        });
    }
    
    async run(message, args) {
        message.channel.sendMessage("Alright");
        var chance = Math.floor(Math.random() * 2);
        if (chance == 0) {
            message.reply("It's heads")
        } else {
            message.reply("It's tails");
        }
    }
}
module.exports = CoinFlipCommand;