const commmando = require("discord.js-commando");
const discord = require("discord.js");
class AboutCommand extends commmando.Command {
    constructor (client) {
        super(client, {
            name: "about",
            group: "simple",
            memberName: "about",
            description: "Prints out hard-coded information about the bot. If the color shown on the side of the embed is purple, you are running an unstable release. If it is blue, you are running a stable release. If it is red, Yeet Assistant is nearing the end of it's life and should start being phased out."
        });

    }
    async run(message, args) {
        var actionSummary = new discord.RichEmbed()
        .setTitle("About Yeet Assistant")
        .setDescription("Yeet Assistant Release one.")
        .setFooter("Copyright 2019 CMWAidanx.")
        .setImage("https://cdn.discordapp.com/attachments/536995348384579585/537038906731003915/yeet.png")
        .setAuthor("Yeet Assistant", "https://cdn.discordapp.com/attachments/536995348384579585/537038906731003915/yeet.png")
        .setColor(0xae1dc1);
        message.channel.sendEmbed(actionSummary);
    }
}
module.exports = AboutCommand;