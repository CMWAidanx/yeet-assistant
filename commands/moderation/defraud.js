const commmando = require("discord.js-commando");

class DefruadCommand extends commmando.Command {
    constructor (client) {
        super(client, {
            name: "defraud",
            group: "moderation",
            memberName: "defraud",
            description: "A command that can be used by the server owner to prove they are indeed the owner. Mostly pointless"
        });

    }
    async run(message, args) {
        message.channel.sendMessage("You are indeed the owner.");
    }
}
// module.exports = DefruadCommand