const commando = require("discord.js-commando");
const discord = require("discord.js");
const as = require("../../actionsummary");
const abuselist = require("../../safety/abuselist");
const dmas = require("../../dm-actionsummary");
// const notice = require("../../notice");
var bugsnag = require('bugsnag');
var bugsnagClient = bugsnag.register('ab078f4d85e5ca950344369f1bf24168');
var tmsa = require("./messages/this-message-shouldnt-appear.json");
var em = require("./messages/errors.json");
var asm = require("./messages/action-summary.json");
class KickCommand extends commando.Command {
    constructor(client) {
        super(client,{
            name: 'kick',
            group: 'moderation',
            memberName: 'kick',
            description: 'Kicks a player by a mention. After the mention, add a reason. For example, !kick <user> i said so will kick <user> because i said so.'
        });
    }
    async run(message, args) {
        if (abuselist.onBlacklist(message.guild.member(message.member))) {
            message.reply(em.abusedlistedkick);
            return;
        }
        if (message.mentions.users.first() == null) {
            message.channel.sendMessage(em.notfoundkick);
            return;
        }
        let kickedUser = message.guild.member(message.mentions.users.first());
        if (!kickedUser) {
            message.channel.sendMessage(tmsa.message4);
            return;
        }
        if (kickedUser == null ) {
            message.channel.sendMessage(tmsa.message3);
            return;
        } if (message.guild.member(kickedUser) == null) {
            message.channel.sendMessage(em.notfoundkick);
            return;
        }
        if (!message.member.hasPermission("KICK_MEMBERS")) {
            message.reply(em.nopermskick);
            return;
        }
        let words = args.split(' ');
        let reason = words.slice(1).join(' ');
        dmas.sendSummary(kickedUser, asm.actionkick, reason, "http://yeetassistant.rf.gd/icon/ya-kick.png");
        console.log("Kicked " +  message.guild.member(kickedUser).displayName + " for " + reason);
       try {
        message.guild.member(kickedUser).kick(reason)
        .then(as.sendSummary(asm.actionkick, asm.done, asm.reason + reason, message.guild.member(kickedUser).displayName, message.channel, 0x21ff00, "http://yeetassistant.rf.gd/icon/ya-kick.png", message.guild));
    } catch (e) {
        bugsnag.notify(e);
    }
}
    
}
module.exports = KickCommand;