const commando = require("discord.js-commando");
const discord = require("discord.js");
const as = require("../../actionsummary");
const abuselist = require("../../safety/abuselist");
const dmas = require("../../dm-actionsummary");
var bugsnag = require('bugsnag');
var tmsa = require("./messages/this-message-shouldnt-appear.json");
var asm = require("./messages/action-summary.json");
var em = require("./messages/errors.json");
var bugsnagClient = bugsnag.register('ab078f4d85e5ca950344369f1bf24168');
class BanCommand extends commando.Command {
    constructor(client) {
        super(client,{
            name: 'ban',
            group: 'moderation',
            memberName: 'ban',
            description: 'bans a player by a mention. After the mention, add a reason. For example, !ban <user> because i said so will ban <user> because i said so.'
        });
    }
    async run(message, args) {
        if (abuselist.onBlacklist(message.guild.member(message.member))) {
            message.reply(em.abuselistedban);
            return;
        }
        let bannedUser = message.guild.member(message.mentions.users.first());
        if (!bannedUser) {
            message.channel.sendMessage(em.notfoundban);
            return;
        }
        if (!message.member.hasPermission("BAN_MEMBERS")) {
            message.reply(em.nopermsban);
            return;
        }
        let words = args.split(' ');
        let reason = words.slice(1).join(' ');
        // var actionSummary = new discord.RichEmbed()
        // .setTitle("Action Summary")
        // .setDescription("Action: Ban" + "\nAffected Member(s): " + message.guild.member(bannedUser).displayName + "\nReason: " + reason + "\nStatus: done")
        // .setColor(0x36bc2f);
        // message.channel.sendEmbed(actionSummary);
        console.log("banned " +  message.guild.member(bannedUser).displayName + " for " + reason);
        dmas.sendSummary(bannedUser, asm.actionban, reason, "http://yeetassistant.rf.gd/icon/ya-ban.png");
        try {
        message.guild.member(bannedUser).ban(reason)
        .then(as.sendSummary(asm.actionban, asm.done, asm.reason + reason, message.guild.member(bannedUser).displayName, message.channel, 0x36bc2f, "http://yeetassistant.rf.gd/icon/ya-ban.png", message.guild));
        } catch (e) {
            message.reply(tmsa.message0);
            bugsnag.notify(e);
        }
    }
}
module.exports = BanCommand;