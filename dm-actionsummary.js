const discord = require("discord.js");
module.exports = {
    sendSummary: function(member, action, reason, icon) {
        var summary = new discord.RichEmbed()
            .setAuthor("Action Summary: Notice", icon)
            .setTitle("The following action occured that involves you: " + action)
            .setDescription("Action: " + action + "\nReason: " + reason)
            .setFooter("Please do not reply to this message.")
            .setColor(0x0037ff);
        member.sendEmbed(summary);
    }
}