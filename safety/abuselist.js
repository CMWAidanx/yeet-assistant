const fs = require('fs');
var abuselist = require("./abuselist.json");
function containsObject(obj, list) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (list[i] === obj) {
            return true;
        }
    }

    return false;
}
module.exports = {
    onBlacklist: function(player) {
        if (containsObject(player.id, abuselist.ids))  {
            return true;
        } else return false;
    }
    
}